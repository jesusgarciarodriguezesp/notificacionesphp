<?php
/*
 *  Servicio de disparo de notificaciones .
 *  Un servicio externo  requere que se lance una notificacion y a una determinada hora 
 *  llama a este servicio  y  pasa  la url del contenido de la notificación EN UN POST .  
 *  las peticiones se guardan en una tabla que registra la url y la hora de petición .  
 *  Un proceso posterior esta monitorizando la tabla  que contiene ademas un campo estado y un campo comentarios  
 *  aquellos que no esten en estado done se ejecutan y se guarda en el campo comentario un extracto del mensaje enviado si  hay un error se guarda estado error y se se guarda en el estado comentarios el problema .
 * 
 *  CREATE TABLE `orniguiasiberia_bbdd`.`notify_gc` ( `id_work` SERIAL NOT NULL , `work_url` VARCHAR(256) NOT NULL , `status` TINYINT NOT NULL DEFAULT '0' , `comments` VARCHAR(512) NULL ,`not_ts` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id_work`) ) ENGINE = InnoDB;
 *
 *  sis e quiere especificar que solo e ejecute una app se cpone en el campo allapps si nos e enviara a todas las apps registradas . 
 *  
 *  ALTER TABLE `notify_gc` ADD `appcode` VARCHAR(64) NOT NULL DEFAULT 'allapps' AFTER `id_work`;
 *  
 *  
 *  Ojo la url maxima es de 256 caracteres , si se requiere mas habria que tener un metodo alternativo de  creacion de trabajos , como un volcado a fichero ... 
 *  Debe existir previamente
 *
 */

// Carga fichero propiedades . 
$array_ini = parse_ini_file("config.ini", true);
$servername = "localhost";
$dbportnumber = "1521";
$username = "username";
$password = "password";
$dbname = "myDB";
$work = $_POST["work"];
$appcode = $_POST["appcode"];
$debug_in_screen = "y";
$enable_get = "y";
$trace_level = "all";
$url_suffix="emptyurl";
$conn ;


if ( empty($array_ini))
{
	print_r("ERROR-no-INI");
}
else
{
	if(!empty($array_ini["TRACE"]))
	if($array_ini["TRACE"]["debug_in_screen"]=="y")
	{
		print_r($array_ini);
		//print_r($array_ini["TRACE"]["debug_in_screen"]);


	}
	else $debug_in_screen = "n";
	
	if(!empty($array_ini["NOTIFY"]))
		if(!empty($array_ini["NOTIFY"]["urlcmssufix"]))
		{
			
			$url_suffix=$array_ini["NOTIFY"]["urlcmssufix"];
			print_r($url_suffix);
			//print_r($array_ini["TRACE"]["debug_in_screen"]);
	
	
		}
	else
	$url_suffix="file:/";	

	/*
	 * Trabajo por defecto y app a la que va el trabajo 
	 * el codigo de trabajo allapps se usará para enviar atodas 
	 * 
	 * 
	 */

if(empty($work) and !empty($array_ini["TRACE"])  and !empty($array_ini["TRACE"]["enable_get"]) )
{
	$work = $_GET["work"];
	if(empty($work))
	{
		$work="/index.php";
	}	
} ;

if(empty($appcode) and !empty($array_ini["TRACE"])  and !empty($array_ini["TRACE"]["enable_get"]) )
{
	$appcode = $_GET["appcode"];
	if(empty($appcode))
	{
		$appcode="allapps";
	}
} ;

if(empty($array_ini["TRACE"])  and !empty($array_ini["TRACE"]["trace_level"]) )
{
	$trace_level = $array_ini["TRACE"]["trace_level"];
} else $trace_level = "all";

if( $trace_level == "all")
{
	printf("Trace all issues");
}

if(empty($array_ini["NOTIFY"])  and !empty($array_ini["NOTIFY"]["urlcmssufix"]) )
{
	$trace_level = $array_ini["TRACE"]["trace_level"];
} 

$servername = $array_ini["DB"]["host"];
$username = $array_ini["DB"]["user"];
$dbportnumber = $array_ini["DB"]["port"];
$password = $array_ini["DB"]["password"];
$dbname = $array_ini["DB"]["schema"];


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname,$dbportnumber);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}
// 

if( $trace_level == "all")
{
	printf("Con is done");
}


 
	if( $trace_level == "all")
	{
		printf(" Insert %s",$work);
	}	
	
  //  Insert new item
  $insertsql = sprintf("INSERT INTO orniguiasiberia_bbdd.notify_gc (work_url,appcode,not_ts) VALUES ('%s%s','%s',now())",$url_suffix,$work,$appcode); 
  if( $trace_level == "all")
  {
  	printf(" Insert query %s",$insertsql);
  }
  

  
  if ($conn->query($insertsql) === TRUE) {
  	echo "New record created successfully";
  } else {
  	echo "Error: " . $sql . "<br>" . $conn->error;
  }
 
	



$conn->close();
}

if (! empty($conn))
{
	$conn = null;	
}	
	


?>
